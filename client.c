#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "wsocks.h"

#define BUFFER_LENGTH 1024
#define ADDRESS "192.168.0.1"

int main(int argc, char *argv[])
{
	struct wsock ws;
	char readbuffer[BUFFER_LENGTH];
	char *sendbuffer = "A test send string\n";

	if (ws_socket(&ws, 0) != 0) {
		printf("Failed creating a socket\n");
		exit(1);
	}

	printf("Socket up ... ");

	if (ws_connect(&ws, ADDRESS) != 0) {
		printf("Socket connection failed.\n");
		exit(1);
	}

	ws_close(&ws);
	if (ws_send(&ws, sendbuffer, strlen(sendbuffer)) == -1) {
		printf("Send failed\n");
		return 1;
	}

	if (ws_recm(&ws) == NULL) {
		printf("Message crapped\n");
		return 1;
	}

	struct ws_msg *m = &(ws.message);
	ws_print_msg_head(&ws);

	char resp[BUFFER_LENGTH];
	int reclen;

	if ((reclen = ws_recv(&ws, resp, BUFFER_LENGTH - 1)) == -1) {
		printf("More bad things\n");
		return 1;
	}
	resp[reclen] = '\0';

	printf("Message 1:\n%s\n", resp);
	if (ws_recm(&ws) == NULL) {
		printf("Second message crapped\n");
		return 1;
	}

	if ((reclen = ws_recv(&ws, resp, BUFFER_LENGTH - 1)) == -1) {
		printf("Receiving message failed\n");
		return 1;
	}
	resp[reclen] = '\0';
	printf("Message 2:\n%s\n", resp);

	ws_free(&ws);

	return 0;
}
