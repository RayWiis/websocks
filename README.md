# Websockets client

A minimal and out of spec implementation of the websockets protocol client in C.

Originally written as a university assignment for network programming.

## Getting started

To build the application you need a C compiler and make.

Running

```sh
make
```

Should build the files and produce the ws executable.
