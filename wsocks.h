#ifndef _WSOCKS_INCLUDE
#define _WSOCKS_INCLUDE

#include <stdint.h>

#define WS_GUID "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define WS_DEFAULT_PORT 80

#define WS_OPCODE_CONT 0
#define WS_OPCODE_TEXT 1
#define WS_OPCODE_BIN 2
#define WS_OPCODE_CLOSE 8
#define WS_OPCODE_PING 9
#define WS_OPCODE_PONG 10

#define WS_STATE_NOT_CONNECTED 1
#define WS_STATE_CONNECTING 2
#define WS_STATE_OPEN 3
#define WS_STATE_CLOSED 4

/**
 * Message header struct
 */
struct ws_msg
{
	uint8_t		fin;
	uint8_t		rsv1;
	uint8_t		rsv2;
	uint8_t		rsv3;
	uint8_t		opcode;
	uint8_t		mask_bit;
	uint64_t	payload_len;
	char 		mask_key[4];

	uint64_t	read;
};

/**
 * Basic websocket struct
 */
struct wsock
{
	int state;
	int stream_sock;
	int port;
	char *host;
	char *key;

	struct ws_msg message;
	
	int buff_len;
	int buff_it;
	char *buff;
};


/**
 * ws_socket() - initialize an empty wsock object
 *
 * @s websocket object
 * Returns:
 * 	0 if initialization was succcesfull
 * 	-1 If otherwise
 */
int ws_socket(struct wsock *s, int port);

/**
 * ws_free() - Free WebSocket resources
 *
 * @s WebSocket to free
 */
void ws_free(struct wsock *s);

/**
 * ws_connect() - starts a websocket connection
 *
 * @s socket struct
 * @address text address in dots and numbers notation
 */
int ws_connect(struct wsock *s, char *address);

/**
 * ws_recm() - read out message header
 *
 * @s socket struct
 *
 * Returns:
 *	NULL if failed
 */
struct ws_msg *ws_recm(struct wsock *s);

/**
 * ws_recm() - Receive a WebSocket message
 *
 * @m Message struct
 * @d destination
 * @len bytes to read
 *
 * Returns: length of the message
 * -1 on error
 */
int ws_recv(struct wsock *s, char *d, int len);

/**
 * ws_send() - Send a text message with websockets
 *
 * @s WebSocket to use
 * @buff Buffer to send
 * @len Length of message to send
 *
 * Returns: the ammount of bytes sent in a message
 */
int ws_send(struct wsock *s, char *buff, uint64_t len);

void ws_close(struct wsock *s);

/**
 * ws_print_msg_head() - Print out the fields in a ws_msg
 *
 * @s wsock of message
 */
void ws_print_msg_head(struct wsock *s);

#endif

