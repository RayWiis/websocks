#include "wsocks.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <openssl/sha.h>
#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

#define WS_MSG_FLAG_FIN		0b10000000
#define WS_MSG_FLAG_RSV1	0b01000000
#define WS_MSG_FLAG_RSV2	0b00100000
#define WS_MSG_FLAG_RSV3	0b00010000
#define WS_MSG_FLAG_OPCODE	0b00001111

#define WS_MSG_FLAG_MASK	0b10000000
#define WS_MSG_FLAG_LENGTH	0b01111111


#define BUFFER_LENGTH		1024

/**
 * Header data container struct
 */
struct ws_msg_data
{
	char op1;
	char op2;
};

/**
 * base64() - Encode base 64 strings with openssl
 * @input String to encode
 * @length length of string
 */
char *base64(const unsigned char *input, int length)
{
	BIO *bmem, *b64;
	BUF_MEM *bptr;

	b64 = BIO_new(BIO_f_base64());
	bmem = BIO_new(BIO_s_mem());
	b64 = BIO_push(b64, bmem);
	BIO_write(b64, input, length);
	BIO_flush(b64);
	BIO_get_mem_ptr(b64, &bptr);

	char *buff = (char *)malloc(bptr->length);
	memcpy(buff, bptr->data, bptr->length-1);
	buff[bptr->length-1] = 0;

	BIO_free_all(b64);

	return buff;
}

/**
 * sendall() - Send all bytes
 * @s socket
 * @buf buffer to send
 * @len length of buffer
 */
int sendall(int s, char *buf, int len)
{
	int bytes_sent = 0;
	int bytes_total = len;
	int bytes_left = len;
	int n;

	while(bytes_sent < bytes_total) {
		n = send(s, buf+bytes_sent, bytes_left, 0);
		if (n == -1) { break; }
		bytes_sent += n;
		bytes_left -= n;
	}

	return n==-1?-1:0; // return -1 on failure, 0 on success
}

/**
 * ws_zero() - zero the struct
 *
 * @s socket struct to zero
 */
void ws_zero(struct wsock *s)
{
	s->state = WS_STATE_NOT_CONNECTED;
	s->stream_sock = 0;
	s->port = WS_DEFAULT_PORT;
	s->host = NULL;
	s->key = NULL;
	s->buff_it = 0;
	s->buff_len = 0;
	s->buff = NULL;
}

/**
 * ws_socket() - Initialize an empty websocket connection oject
 *
 * @port port to use for connection
 */
int ws_socket(struct wsock *s, int port)
{
	ws_zero(s);

	if (port != 0)
		s->port = port;

	s->stream_sock = socket(AF_INET, SOCK_STREAM, 0);
	if (s->stream_sock < 0)
		return -1;

	s->buff = malloc(sizeof(char) * BUFFER_LENGTH);
	if (s->buff == NULL)
		return -1;

	return 0;
}

/**
 * ws_free() - Free WebSocket resources
 *
 * @s WebSocket to free
 */
void ws_free(struct wsock *s) 
{
	assert(s != NULL);
	free(s->buff);
}


/**
 * ws_handshake() - build a http handshake string
 *
 * @key key string
 * @ip IP address string
 * @path URI resource path
 * @port Port of connection
 *
 * Returns: String of the http handshake for websockets
 */
// TODO: Just update the struct to contain these and use that
char *ws_handshake(char *key, char *ip, char *uri, int port)
{
	// TODO: Add non default ports
	// TODO: Dont be retarded use snprintf
	char method[] = "GET ";
	char version[] = " HTTP/1.1\r\nHost: ";
	char param1[] = "\r\nConnection: Upgrade\r\n";
	char param2[] = "Upgrade: websocket\r\n";
	char ws_ver[] = "Sec-WebSocket-Version: 13\r\n";
	char ws_key[] = "Sec-WebSocket-Key: ";
	char end[] = "\r\n\r\n";
	
	/** total length of the handshake */
	int total_len = strlen(method) + 1;
	total_len += strlen(key) + strlen(ip) + strlen(uri);
	total_len += strlen(version) + strlen(param1) + strlen(param2);
	total_len += strlen(ws_ver) + strlen(ws_key) + strlen(end);

	/** handshake */
	// TODO: Use sprintf
	char *hs = malloc(total_len * sizeof(char));
	/** Do NOT change this order */
	strcpy(hs, method);
	strcat(hs, uri);
	strcat(hs, version);
	strcat(hs, ip);
	strcat(hs, param1);
	strcat(hs, param2);
	strcat(hs, ws_ver);
	strcat(hs, ws_key);
	strcat(hs, key);
	strcat(hs, end);

	return hs;
}

/**
 * ws_accept_hash() - Accept string for a websocket
 * 
 * @key request key
 *
 * Returns: returns hash string
 */
unsigned char *ws_accept_hash(char *key)
{
	int hstr_len = strlen(key) + strlen(WS_GUID) + 1;
	char *hstr = malloc(hstr_len * sizeof(char));
	unsigned char hash[SHA_DIGEST_LENGTH];

	strcpy(hstr, key);
	strcat(hstr, WS_GUID);

	SHA1(hstr, hstr_len - 1, hash);

	char *b64hash = base64(hash, strlen(hash));

	free(hstr);

	return b64hash;
}

/**
 * ws_buff() - Buffer websocket things
 *
 * @s
 *
 * Returns:
 * Length of buffer read or -1 on error
 */
int ws_buff(struct wsock *s)
{
	s->buff_it = 0;
	s->buff_len = recv(s->stream_sock, s->buff, BUFFER_LENGTH - 1, 0);
	if (s->buff_len == -1)
		return -1;

	s->buff[s->buff_len] = '\0';

	return s->buff_len;
}

/**
 * ws_connect() - Initiate a websocket connection
 *
 * @sock socket on which to start the connection
 * @sa Sever address details
 * @size size of sockaddr
 */
int ws_connect(struct wsock *s, char *address)
{
	s->state = WS_STATE_CONNECTING;

	struct sockaddr_in servaddr;

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(s->port);
	servaddr.sin_addr.s_addr = inet_addr(address);

	int sock_stat = connect(
			s->stream_sock,
			(struct sockaddr *) &servaddr,
			sizeof(servaddr));

	if (sock_stat != 0)
		return -1;

	// TODO: Generate key
	char key[] = "aBQdghWgnsm76Cfn5ftqN==";
	char *hs = ws_handshake(key, address, "/", s->port);

	sendall(s->stream_sock, hs, strlen(hs));

	unsigned char *hash = ws_accept_hash(key);
	char res_line[BUFFER_LENGTH];

	printf("My hash: %s\n", hash);

	if (ws_buff(s) == -1)
		return -1;

	// TODO: Parse response && verify server access token
	// FIXME: Scale down on received data to verify connection
	// TODO: Remove this
	printf("----\nResponse:\n%s\n----\n", s->buff);

	free(hs);
	free(hash);

	s->state = WS_STATE_OPEN;

	return 0;
}

/**
 * read_header() - read out and parse  message header
 *
 * Returns: 
 *	0 if all is good
 *	otherwise failed
 */
int read_header(struct wsock *s, struct ws_msg *m)
{
	struct ws_msg_data d;

	// Read first 2 bytes for socket
	int reclen = recv(s->stream_sock, &d, 2, 0);
	// Just assume we're dead
	if (reclen != 2)
		return -1;

	// 4 bits
	m->fin = d.op1 & WS_MSG_FLAG_FIN;
	m->rsv1 = d.op1 & WS_MSG_FLAG_RSV1;
	m->rsv2 = d.op1 & WS_MSG_FLAG_RSV2;
	m->rsv3 = d.op1 & WS_MSG_FLAG_RSV3;
	// 4 more bits
	m->opcode = d.op1 & WS_MSG_FLAG_OPCODE;
	// 1 bit - masking bit
	m->mask_bit = d.op2 & WS_MSG_FLAG_MASK;
	// 7 bytes of len
	m->payload_len = d.op2 & WS_MSG_FLAG_LENGTH;

	if (m->payload_len == 126) {
		uint16_t exlen;
		reclen = recv(s->stream_sock, &exlen, 2, 0);

		if (reclen != 2)
			return -1;

		m->payload_len = ntohs(exlen);
	} else if (m->payload_len == 127) {
		uint64_t exlen;
		reclen = recv(s->stream_sock, &exlen, 8, 0);
		
		if (reclen != 8)
			return -1;
		
		m->payload_len = ntohl(exlen);
	}

	// TODO Consume control opcode messages

	return 0;
}

/**
 * ws_recm() - Receive a WebSocket message
 *
 * @s socket struct
 *
 * Returns:
 *	NULL if failed
 */
struct ws_msg *ws_recm(struct wsock *s)
{
	assert(s != NULL);
	if (s->state != WS_STATE_OPEN)
		return NULL;

	struct ws_msg *m = &(s->message);	

	if (read_header(s, m) != 0)
		return NULL;
	m->read = 0;

	return m;
}

/**
 * Calculate how many characters remain in current message
 *
 * @s wsock of the message
 *
 * Returns: ammount of bytes left in the message
 */
uint64_t ws_calc_msg_remains(struct wsock *s)
{
	struct ws_msg *m = &(s->message);

	return m->payload_len - m->read;
}

/**
 * ws_recv() - reads from a websocket connection
 *
 * @m Message struct
 * @d destination
 * @len bytes to read
 *
 * Returns:
 *	Number of read bytes
 * 	0 Probably means that the message is over
 * 	-1 on error
 */
int ws_recv(struct wsock *s, char *d, int len)
{
	assert((s != NULL) && (d != NULL));

	if (s->state != WS_STATE_OPEN)
		return -1;

	struct ws_msg *m = &(s->message);
	int reclen;

	uint64_t remains = ws_calc_msg_remains(s);
	if (remains == 0) {
		if (m->fin) {
			return 0;
		} else {
			ws_recm(s);
			remains = ws_calc_msg_remains(s);
		}
	}

	if (len > remains) {
		reclen = recv(s->stream_sock, d, remains, 0);

		if (reclen == -1)
			return -1;

		m->read += reclen;
		return reclen;
	} else { // We have more or exactly as much message to read
		reclen = recv(s->stream_sock, d, len, 0);

		if (reclen == -1)
			return -1;

		m->read += reclen;
		return reclen;
	}
}

void ws_mask(char *dest, char *buff, char *mask, int len)
{
	int i;
	for(i = 0; i < len; i++) {
		dest[i] = buff[i] ^ mask[i % 4];
	}

	return;
}

/**
 */
int ws_send(struct wsock *s, char *buff, uint64_t len)
{
	assert((s != NULL) && (buff != NULL));

	if (s->state != WS_STATE_OPEN)
		return -1;

	int headlen;

	if (len < 126) {
		headlen = 2;
	} else if (len < 32767) {
		headlen = 4;
	} else {
		// TODO: Add support for max len msgs
		return -1;
	}

	int msglen = len + headlen + 4;
	char *msg = malloc(sizeof(char) * msglen);

	if (msg == NULL)
		return -1;

	char *mask = "1234";

	// Fin flag and opcode text
	msg[0] = 0b10000001;
	// Adjust length
	if (len < 126) {
		msg[1] = 0b10000000 | ((uint8_t) len);
	} else if (len < 32767) {
		msg[1] = 0b11111110;
		uint16_t hlen = htons((uint16_t) len);
		memcpy(&(msg[2]), &hlen, 2);
	}
	// Add mask
	memcpy(&(msg[headlen]), mask, 4);
	ws_mask(&(msg[headlen + 4]), buff, mask, len);

	sendall(s->stream_sock, msg, msglen);

	free(msg);

	return 0;
}

/**
 * ws_print_msg_head() - Print out the fields in a ws_msg
 *
 * @s wsock of message
 */
void ws_print_msg_head(struct wsock *s)
{
	assert(s != NULL);

	struct ws_msg *m = &(s->message);
	printf("Message header-----------\n");
	printf(" Location | Usage | Value\n");
	printf("   0    0 | Fin   | %u\n", m->fin);
	printf("   0    1 | Rsv1  | %u\n", m->rsv1);
	printf("   0    2 | Rsv2  | %u\n", m->rsv2);
	printf("   0    3 | Rsv3  | %u\n", m->rsv3);
	printf("   0  4-7 | Opcode| %u\n", m->opcode);
	printf("   1    8 | Mask  | %u\n", m->mask_bit);
	printf("   1  ... | Length| %lu\n", m->payload_len);
	printf("-------------------------\n");
}

void ws_close(struct wsock *s)
{
	char msg[8] = {
		0b10001000,
		0b10000010,
		'4',
		'3',
		'2',
		'1'
	};

	uint16_t close_code = 1000;
	close_code = htons(close_code);

	memcpy(&(msg[6]), &close_code, 2);

	ws_mask(&(msg[6]), &(msg[6]), &(msg[2]), 2);
	sendall(s->stream_sock, msg, 8);
	// TODO: Accept a closing verifier
	s->state = WS_STATE_CLOSED;
}

